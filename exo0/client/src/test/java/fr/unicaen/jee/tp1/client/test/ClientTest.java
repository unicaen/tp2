package fr.unicaen.jee.tp1.client.test;

import org.junit.*;
import fr.unicaen.jee.tp1.client.*;
import fr.unicaen.jee.tp1.logic.api.*;


public class ClientTest {
	
	@Test
	public void testAddition() throws CalculetteException {
		Calculette calculette = new CalculetteClient("http://localhost:8080/calc-service-4.0");
		Assert.assertEquals(10, calculette.addition(5, 5), 0);
		Assert.assertEquals(0, calculette.addition(5, -5), 0);
		Assert.assertEquals(-10, calculette.addition(-5, -5), 0);
	}
	
	// etc.
	
}
