package fr.unicaen.jee.tp1.client;

import java.io.*;
import java.net.*;
import java.util.*;
import fr.unicaen.jee.tp1.logic.api.*;


public class CalculetteClient implements Calculette {
	
	private String baseURL;
	private Proxy proxy;
	
	
	public CalculetteClient(String baseURL) {
		this(baseURL, Proxy.NO_PROXY);
	}
	
		
	public CalculetteClient(String baseURL, Proxy proxy) {
		this.baseURL = baseURL;
		this.proxy = proxy;
	}
	

	public double addition(double x, double y) throws CalculetteException, IllegalArgumentException {
		return operation("addition", x, y);
	}

	
	public double soustraction(double x, double y) throws CalculetteException, IllegalArgumentException {
		return operation("soustraction", x, y);
	}

	
	public double multiplication(double x, double y) throws CalculetteException, IllegalArgumentException {
		return operation("multiplication", x, y);
	}

	
	public double division(double x, double y) throws CalculetteException, IllegalArgumentException {
		return operation("division", x, y);
	}
	
	
	private double operation(String op, double x, double y) throws CalculetteException, IllegalArgumentException {
		try {
			URL url = new URL(String.format(Locale.ROOT, "%s/%s?gauche=%f&droite=%f", this.baseURL, op, x, y));
			URLConnection connection = url.openConnection(this.proxy);
			String result = consume(connection);
			return Double.parseDouble(result);
		}
		catch(MalformedURLException e) {
			throw new CalculetteException(e);
		}
		catch(IOException e) {
			throw new CalculetteException(e);
		}
	}
	
	
	public String consume(URLConnection connection) throws IOException {
		StringBuilder result = new StringBuilder();
		String encoding = connection.getContentEncoding() != null ? connection.getContentEncoding() : "UTF-8";
		Reader reader = new InputStreamReader(connection.getInputStream(), encoding);
		for(int c = reader.read(); c > 0; c = reader.read())
			result.append((char)c);
		return result.toString();
	}
	
}
