package fr.unicaen.jee.tp1.servlet;

import fr.unicaen.jee.tp1.logic.api.*;
import fr.unicaen.jee.tp1.logic.impl.*;

public class Soustraction extends AbstractCalculetteServlet {
			
	@Override
	protected double calcule(double gauche, double droite) throws CalculetteException {
		return CalculetteFactory.getCalculette().soustraction(gauche, droite);
	}

}
