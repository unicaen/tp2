# TP2 #

Le premier objectif de ce TP est de se familiariser avec l'utilisation de Maven tout en refactorant le code du TP1 pour lui donner une forme modulaire (exercice 0).
Le second objectif est de se familiariser avec le framework Spring, en l'utilisant d'une part en remplacement de l'approche Servlet utilisée dans le TP1 (exercice 1), et d'autre part en utilisant l'injection de dépendances à la place de la factory (exercice 2).

Voir l'énoncé complet sur le [Moodle](http://foad2.unicaen.fr/moodle/course/view.php?id=24560).