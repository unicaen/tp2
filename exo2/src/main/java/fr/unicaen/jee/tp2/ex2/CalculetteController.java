package fr.unicaen.jee.tp2.ex2;

import fr.unicaen.jee.tp1.logic.api.*;
import org.springframework.web.bind.annotation.*;


@RestController
public class CalculetteController {
	
	private final Calculette backend; // va être injectée automatiquement, voir spring-config.xml
	
	public CalculetteController(Calculette calculette) throws CalculetteException {
		this.backend = calculette;
	}
	
	@RequestMapping("/addition")
    public double addition(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.addition(gauche, droite);
    }
	
	
	@RequestMapping("/soustraction")
    public double soustraction(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.soustraction(gauche, droite);
    }
	
	
	@RequestMapping("/multiplication")
    public double multiplication(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.multiplication(gauche, droite);
    }
	
	
	@RequestMapping("/division")
    public double division(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.division(gauche, droite);
    }
	
}
