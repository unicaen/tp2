package fr.unicaen.jee.tp2.ex2;

import org.springframework.boot.builder.*;
import org.springframework.boot.web.support.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;


@SpringBootApplication
@ImportResource("classpath:spring-config.xml")
public class CalculetteApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CalculetteApplication.class);
    }

}

