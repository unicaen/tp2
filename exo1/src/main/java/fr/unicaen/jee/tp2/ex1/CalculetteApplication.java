package fr.unicaen.jee.tp2.ex1;

import org.springframework.boot.builder.*;
import org.springframework.boot.web.support.*;
import org.springframework.boot.autoconfigure.*;


@SpringBootApplication
public class CalculetteApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CalculetteApplication.class);
    }

	// Seulement nécessaire pour déploiement autonome (sans conteneur)
    public static void main(String[] args) throws Exception {
        org.springframework.boot.SpringApplication.run(CalculetteApplication.class, args);
    }

}

