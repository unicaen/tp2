package fr.unicaen.jee.tp2.ex1;

import fr.unicaen.jee.tp1.logic.api.*;
import fr.unicaen.jee.tp1.logic.impl.*;
import org.springframework.web.bind.annotation.*;


@RestController
public class CalculetteController {
	
	private final Calculette backend;
	
	public CalculetteController() throws CalculetteException {
		this.backend = CalculetteFactory.getCalculette();
	}
	
	@RequestMapping("/addition")
    public double addition(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.addition(gauche, droite);
    }
	
	
	@RequestMapping("/soustraction")
    public double soustraction(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.soustraction(gauche, droite);
    }
	
	
	@RequestMapping("/multiplication")
    public double multiplication(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.multiplication(gauche, droite);
    }
	
	
	@RequestMapping("/division")
    public double division(@RequestParam(value="gauche") double gauche, @RequestParam(value="droite") double droite) throws CalculetteException
	{
        return backend.division(gauche, droite);
    }
	
}
