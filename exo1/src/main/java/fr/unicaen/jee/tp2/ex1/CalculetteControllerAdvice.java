package fr.unicaen.jee.tp2.ex1;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;


@ControllerAdvice
public class CalculetteControllerAdvice {
	
	@ExceptionHandler(java.lang.IllegalArgumentException.class)
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)	
	@ResponseBody
	public String handleIllegalArgument(Exception e) { 
		return e.getMessage(); 
	}
		
}
